const { request } = require("express");
const express = require("express");
const converter = require("./converter");
const app = express();
const port = 3000;

// Welcome endpoint
app.get("/", (req, res) => res.send("Welcome"));

// RGB to Hex endpoint
app.get("/rgb-to-hex", (req, res) =>{
    const red = parseInt(req.query.red, 10);
    const green = parseInt(req.query.green, 10);
    const blue = parseInt(req.query.blue,);
    res.send(converter.rgbToHex(red,green,blue));
});

if (process.env.NODE_ENV === "test"){
    module.exports = app;
} else{
    app.listen(port, () => console.log('Server listening'));
}